Collect signatures online by embedding documents as digital forms. Once a form is embedded, signers apply an electronic signature, directly online.

GroupDocs Signature is an efficient digital signature service that is completely web-based. GroupDocs' easy-to-use electronic signature service lets you collect digital signatures without the administrative overhead of printing, envelope stuffing and posting. It saves time and effort.

First, simply add fields and prepare your documents for signing. You can prepare any number of signature forms with fields for signature, date, initials, etc. using the Signature app at GroupDocs.com.Then share the forms with the people who needs to sign them. The signers sign the documents online in a browser. As soon as the document is signed, you are notified.

After installing the plugin, embed forms to your web pages. To embed signature forms you only need the form's ID. Your customers can now sign documents online directly on your website.

GroupDocs Signature gives you a greener, paperless office.

-- REQUIREMENTS --

* Drupal 'Libraries API' module - http://drupal.org/project/libraries

* Drupal 'lightbox2' module - http://drupal.org/project/lightbox2

* Drupal 'jquery_update' module - http://drupal.org/project/jquery_update

* GroupDocs PHP SDK - Download the adapted version for Drupal module from:
  https://github.com/groupdocs/drupal-groupdocs-signature/raw/master/groupdocs-php.zip
  and unpack into your libraries
  location e.g. sites/all/libraries/groupdocs-php

* jQuery File Tree - Download the adapted version for Drupal module from:
  https://github.com/groupdocs/drupal-groupdocs-signature/raw/master/jquery_file_tree.zip
  and unpack into your libraries
  location e.g. sites/all/libraries/jquery_file_tree

-- INSTALLATION --

1. Unpack the groupdocs_signature folder and contents in the appropriate modules
   directory of your Drupal installation. This is probably sites/all/modules/
   Make sure that groupdocs_signature folder is writable.
2. Enable Embedded Groupdocs Signature in the admin modules section.
3. Now you need to configure your Embedded Groupdocs Signature module
   and fill Client ID and API Key. You can find them in your GroupDocs account.
4. Click on "manage fields" under Home » Administration » Structure and
   add new field with type "GroupDocs signature".
5. Go edit or add new node of type that you added new field to. Enter document
   GUID to the text input or click "Choose file" to open pop-up in which you
   will able to document file from your GroupDocs account or upload new one.

   Note: Current version of module support creation of only one field!
