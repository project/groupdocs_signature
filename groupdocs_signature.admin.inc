<?php
/**
 * @file
 * Functions relating to the admin of this module
 */

/**
 * Form constructor for the file uploadingform.
 *
 * @see groupdocs_signature_menu()
 */
function groupdocs_signature_uploading_form($form, &$form_state)
{
    $jquery_file_tree_path = libraries_get_path('jquery_file_tree');

    $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('First name'),
        '#default_value' => '',
        '#size' => 22,
        '#maxlength' => 255,
        '#description' => t("Recipient first name."),
    );

    $form['lastName'] = array(
        '#type' => 'textfield',
        '#title' => t('Last name'),
        '#default_value' => '',
        '#size' => 22,
        '#maxlength' => 255,
        '#description' => t("Recipient last name."),
    );

    $form['email'] = array(
        '#type' => 'textfield',
        '#title' => t('Email'),
        '#default_value' => '',
        '#size' => 22,
        '#maxlength' => 255,
        '#description' => t("Recipient email."),
    );

    $form['groupdocs_upload'] = array(
        '#name' => 'file',
        '#type' => 'file',
        '#title' => t('Choose a file'),
        '#size' => 22,
        '#attached' => array(
            'js' => array(
                $jquery_file_tree_path . '/jquery.file_tree.js' => array('type' => 'file'),
                drupal_get_path('module', 'groupdocs_signature') . '/js/tree_viewer_page.js' => array('type' => 'file'),
            ),
            'css' => array(
                $jquery_file_tree_path . '/jquery.file_tree.css',
                drupal_get_path('module', 'groupdocs_signature') . '/css/groupdocs_signature.css',
            ),
        ),
        '#description' => t("Document for sign."),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Go!'),
    );

    return $form;
}

/**
 * Form submission handler for groupdocs_signature_uploading_form().
 *
 * groupdocs_signature_uploading_form submit process
 * move upload file to module directory
 * call Groupdocs Api to upload file
 * using private and public keys from config page
 * unset file fom module directory.
 *
 * Call javascript function that will pass file guid to
 * parent window's test input
 *
 * @see groupdocs_signature_uploading_form()
 */
function groupdocs_signature_uploading_form_submit($node, &$form_state)
{
    if ($_FILES["file"]['error'] !== 0) {
        print "Error on file uploading";
        exit();
    }

    $email = $_POST['email'];
    $signName = $_POST['lastName'];
    $lastName = $_POST['name'];

    $groupdocsphp_path = libraries_get_path('groupdocs-php');
    require_once $groupdocsphp_path . '/APIClient.php';
    require_once $groupdocsphp_path . '/StorageApi.php';
    require_once $groupdocsphp_path . '/GroupDocsRequestSigner.php';
    require_once $groupdocsphp_path . '/FileStream.php';
    require_once $groupdocsphp_path . '/SignatureApi.php';

    $login = variable_get('groupdocs_signature_client_login');
    $password = variable_get('groupdocs_signature_client_password');
    $basePath = 'https://api.groupdocs.com/v2.0';
//Create signer object
    $signer = new GroupDocsRequestSigner("123");
//Create apiClient object
    $apiClient = new APIClient($signer);
//Creaet Shared object
    $shared = new SharedApi($apiClient);
//Set base path
    $shared->setBasePath($basePath);
//Set empty variable for result
    $result = "";
//Login and get user data
    $userData = $shared->LoginUser($login, $password);
//Check status
    if ($userData->status == "Ok") {
        //If status Ok get all user data
        $result = $userData->result->user;
        $private_key = $result->pkey;
        $user_id = $result->guid;
    } else {
        print t("Please, fill in correctly Client Login and Client Password on 'GD Signature' configuration page.");
    }
    $uploadedFile = $_FILES['file'];
    //Temp name of the file
    $tmp_name = $uploadedFile['tmp_name'];
    //Original name of the file
    $name = $uploadedFile['name'];
    //Creat file stream
    $fs = FileStream::fromFile($tmp_name);

    //###Create Signer, ApiClient and Storage Api objects

    //Create signer object
    $signer = new GroupDocsRequestSigner($private_key);
    //Create apiClient object
    $apiClient = new APIClient($signer);
    //Create Storage Api object
    $apiStorage = new StorageApi($apiClient);

    //###Make a request to Storage API using user_id

    //Upload file to current user storage
    $uploadResult = $apiStorage->Upload($user_id, $name, 'uploaded', null, $fs);

    //###Check if file uploaded successfully
    if ($uploadResult->status == "Ok") {
        //Create SignatureApi object
        $signature = new SignatureApi($apiClient);
        //Create envilope using user id and entered by user name
        $envelop = $signature->CreateSignatureEnvelope($user_id, $name);
        //Add uploaded document to envelope
        $addDocument = $signature->AddSignatureEnvelopeDocument($user_id, $envelop->result->envelope->id, $uploadResult->result->guid);
        //Get role list for curent user
        $recipient = $signature->GetRolesList($user_id);
        //Get id of role which can sign
        for ($i = 0; $i < count($recipient->result->roles); $i++) {
            if ($recipient->result->roles[$i]->name == "Signer") {
                $roleId = $recipient->result->roles[$i]->id;
            }
        }

        //Add recipient to envelope
        $addRecipient = $signature->AddSignatureEnvelopeRecipient($user_id, $envelop->result->envelope->id, $email, $signName, $lastName, $roleId, null);
        //Get recipient id
        $getRecipient = $signature->GetSignatureEnvelopeRecipients($user_id, $envelop->result->envelope->id);
        $recipientId = $getRecipient->result->recipients[0]->id;

        $getDocuments = $signature->GetSignatureEnvelopeDocuments($user_id, $envelop->result->envelope->id);
        $signFieldEnvelopSettings = new SignatureEnvelopeFieldSettings();
        $signFieldEnvelopSettings->locationX = "0.15";
        $signFieldEnvelopSettings->locationY = "0.73";
        $signFieldEnvelopSettings->locationWidth = "150";
        $signFieldEnvelopSettings->locationHeight = "50";
        $signFieldEnvelopSettings->name = $name;
        $signFieldEnvelopSettings->forceNewField = true;
        $signFieldEnvelopSettings->page = "1";
        $addSignField = $signature->AddSignatureEnvelopeField($user_id, $envelop->result->envelope->id, $getDocuments->result->documents[0]->documentId, $recipientId, "0545e589fb3e27c9bb7a1f59d0e3fcb9", $signFieldEnvelopSettings);
        $callBack = ''; //$GLOBALS['base_url'] . "/groupdocs_signature/signature_callback";
        //Send envelop with callback url
        $send = $signature->SignatureEnvelopeSend($user_id, $envelop->result->envelope->id, $callBack); //Url for callback


        $result = array();
        //Make iframe
        $result = $envelop->result->envelope->id . '/' . $recipientId;
    } else {
        $result = "Error";

    }

    print "<script>
    window.parent.jQuery('input[name*=\"groupdocs_envelope_recipient_id\"]').val('" . $result . "');
    window.parent.Lightbox.end()
    </script>";
    drupal_exit();
}


/**
 * Page callback for groupdocs_signature_menu().
 *
 * This page handling callback
 * when document was signed.
 *
 * @see groupdocs_signature_menu()
 */
function groupdocs_signature_callback_page()
{

    $post = $_POST;

    if (!empty($post)) {

        $filename = 'sign.txt';

        $content = '';
        foreach ($post as $key => $value) {
            $content .= $key . ": " . $value;
        }

        $file = file_save_data($content, 'public://' . $filename, FILE_EXISTS_RENAME);
    }
}